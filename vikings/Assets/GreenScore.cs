﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenScore : MonoBehaviour
{
    public int ElementValue = 1;

     void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Green.green.ChangeScore(0 + ElementValue);
        }
    }
}
