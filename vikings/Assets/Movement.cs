﻿

using System;
using UnityEngine;

public class Movement : MonoBehaviour
{
    

    public float moveSpeed = 0f;
    public float jumpHeight = 1f;
    public Animator animator;
    public bool isGrounded = false;
    public bool isDead = false;

    private SpriteRenderer spry;

    private Rigidbody2D rb;
    
    


    void Awake()
    {
        rb = GetComponent<Rigidbody2D>() ;
        spry = GetComponent<SpriteRenderer>();
    }

    
    void Update()
    {
        
        

        Jump();

        
        animator.SetFloat("horizontal", Math.Abs (rb.velocity.x));   // animacja poruszania się na x, czyli dokładnie zmiany x 

        if(rb.velocity.x > 0)
        {
            spry.flipX = false;
        }
        else if (rb.velocity.x < 0)
        {
            spry.flipX = true;
        }





    }

    private void FixedUpdate()
    {
        float direction = Input.GetAxis("Horizontal");

        Vector3 currentVelocity = rb.velocity;

        currentVelocity.x = direction * moveSpeed;

        rb.velocity = currentVelocity;



    }


    void Jump ()
    {
        
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true) //Tojest ważne zeby były dwa ==
        {
            Vector3 currentVelocity = rb.velocity;     //Vector3 staje się zmienną currentVelocity, czyli dokładnie rb.velocity !
            currentVelocity.y = 0;

            rb.velocity = currentVelocity;

            rb.AddForce(new Vector2(0f, jumpHeight), ForceMode2D.Impulse);

            animator.SetBool("isJumping", true); // to jest do animacji, dokładnie to jak jest grounded i wtedy mogę skoczyć, to animacja jest


        }




        if (isGrounded && Mathf.Approximately(rb.velocity.y, 0f))      
        {
            animator.SetBool("isJumping", false);
        }


        


    }
      
     
  
       
        

    
}
