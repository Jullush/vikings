﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dead : MonoBehaviour
{

    GameObject Player;

    public Movement move;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        Player = gameObject.transform.parent.gameObject;
    }

    // Update is called once per frame

    private void OnTriggerStay2D(Collider2D collision)
    {



        if(collision.tag == "DeadlyThings")
        {


            anim.SetBool("isJumping", false);
            
            anim.SetBool("isDead", true);
            
            move.enabled = false;
            StartCoroutine(DeathCo());
        }
    }

    private IEnumerator DeathCo()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(0);
    }
   


}
