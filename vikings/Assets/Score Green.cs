﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreGreen : MonoBehaviour
{
    public static ScoreGreen instance;

    public Text text;
    int greenelementsy = 0;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void ChangeScore(int ElementValue)
    {
        greenelementsy += ElementValue;
        text.text = "x" + greenelementsy.ToString();

    }

}
