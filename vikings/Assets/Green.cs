﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Green : MonoBehaviour
{
    public static Green green;
    public Text text;
    int greenelementsy = 0;

    void Start()
    {
        if (green == null)
        {
            green = this;
        }
    }

    public void ChangeScore(int ElementValue)
    {
        greenelementsy += ElementValue;
        text.text = "x" + greenelementsy.ToString();

    }
}
