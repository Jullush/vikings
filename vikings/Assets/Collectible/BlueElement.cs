﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueElement : MonoBehaviour
{
    public int ElementValue = 1;

     void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Score.instance.ChangeScore(0 + ElementValue);
        }
    }
}
